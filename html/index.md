---
layout: default
show_hero: true
---

# Downloads

* [Latest PDF version of CV (Dutch)](pdf/cv_nl.pdf)
* [Latest PDF version of CV (English)](pdf/cv_en.pdf)
* [Thesis *"OpenBSD, Secure Server OS"* (2004, Dutch)](pdf/verleyev.pdf)

# Technical [![pipeline status](https://framagit.org/vv/pubcv/badges/master/pipeline.svg)](https://framagit.org/vv/pubcv/pipelines)

This page was made using [jekyll](https://jekyllrb.com/) and [Bulma](https://bulma.io/).
The PDF files were constructed using [LaTeX](https://www.latex-project.org/).<br>
Both are rebuilt from source automatically whenever code is pushed, using a
[Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/) pipeline.<br>
Feel free to browse the [source code]({{ site.git_repo }}) repo at any time.

This domain <b>{{ site.website }}</b> uses HTTPS by default using auto-renewal
certificates with [Certbot](https://certbot.eff.org/) and
[LetsEncrypt](https://letsencrypt.org/).<br> The nginx SSL configuration should
score a [Qualys SSL Labs A+ rating](https://www.ssllabs.com/ssltest/analyze.html?d={{ site.website }}&hideResults=on).

The web-server is running in an [LXD container](https://linuxcontainers.org/lxd/)
on Ubuntu Linux Server, deployed using [Ansible](https://www.ansible.com/) playbooks.
